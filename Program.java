import java.util.*;
import java.lang.*;
import java.io.*;
import java.text.*;

public class Program{ 
      
    public static Set<OverlapCoord> getOverlap(List<Interval> intervalList) { 
    if (intervalList == null) { 
     throw new NullPointerException("Input list cannot be null."); 
    } 

    final HashSet<OverlapCoord> hashSet = new HashSet<OverlapCoord>(); 

    for (int i = 0; i < intervalList.size() - 1; i++) { 
     final Interval intervali = intervalList.get(i); 

     for (int j = 0; j < intervalList.size(); j++) { 
      final Interval intervalj = intervalList.get(j); 

      if (intervalj.getStart() < intervali.getEnd() && intervalj.getEnd() > intervali.getStart() && i != j) { 
       hashSet.add(new OverlapCoord(Math.max(intervali.getStart(),intervalj.getStart()), 
              Math.min(intervali.getEnd(), intervalj.getEnd()))); 
      } 
     } 
    } 

    return hashSet; 
} 
}

/*
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Arrays;
public class filetest {
public static void main(String[] args) throws Exception
{
File f = new File("f.txt");
FileReader fr = new FileReader(f);
char[] c=new char[(int) f.length()];
int[] B=new int[(int) f.length()];
fr.read(c,0,(int)f.length());
int len=(int)f.length();
int i;
for (i=0;i<len;i++)
{
B=(int)c;
}
System.out.println(Arrays.toString(B));
for (i=0;i<len;i++)
c=(char)B;
fr.close();
FileWriter secfile = new FileWriter("g.txt");
secfile.write(c);
secfile.close();
}
}
*/